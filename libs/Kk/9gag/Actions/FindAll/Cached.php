<?php

class Kk_9gag_Actions_FindAll_Cached implements Kk_9gag_Actions_FindAll_Interface
{
    private $base;
    private $cache;
    
    public function __construct(Kk_9gag_Actions_FindAll_Interface $base, Zend_Cache_Core $cache)
    {
        $this->base = $base;
        $this->cache = $cache;
    }
    
    final public function findAll($section = Kk_9gag_Domains_Section::HOT, $page = 0)
    {
        $gags = array();
        
        $cacheId = "9gags_{$section}_{$page}";
        if (($gags = $this->cache->load($cacheId)) === false) {
            $gags = $this->base->findAll($section, $page);
            
            if (count($gags) > 0)
                $this->cache->save($gags, $cacheId);
        } 
        
        return $gags;
    }
}