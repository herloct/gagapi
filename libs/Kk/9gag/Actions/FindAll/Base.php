<?php

class Kk_9gag_Actions_FindAll_Base implements Kk_9gag_Actions_FindAll_Interface
{
    private $repositories;
    
    public function __construct(Kk_9gag_Repositories_FindAll_Interface $repositories)
    {
        $this->repositories = $repositories;
    }
    
    final public function findAll($section = Kk_9gag_Domains_Section::HOT, $page = 0)
    {
        return $this->repositories->findAll($section, $page);
    }
}