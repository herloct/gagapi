<?php

class Kk_9gag_Actions_FindAll_ClientCached implements Kk_9gag_Actions_FindAll_Interface
{
    private $base;
    private $lifetime;
    
    public function __construct(Kk_9gag_Actions_FindAll_Interface $base, $lifetime)
    {
        $this->base = $base;
        $this->lifetime = $lifetime;
    }
    
    final public function findAll($section = Kk_9gag_Domains_Section::HOT, $page = 0)
    {
        if (! headers_sent()) header('Cache-Control: public, max-age=60');
        
        return $this->base->findAll($section, $page);
    }
}