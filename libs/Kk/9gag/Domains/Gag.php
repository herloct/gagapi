<?php

class Kk_9gag_Domains_Gag
{
    public $id;
    public $url;
    public $title;
    public $image;
    public $time;
    public $votes;
    public $comments;
    
    public static function create(
        $id,
        $url,
        $title,
        Kk_9gag_Domains_Image $image,
        $time,
        $votes,
        $comments
    ) {
        $gag = new Kk_9gag_Domains_Gag;
        $gag->id = (float) $id;
        $gag->url = (string) $url;
        $gag->title = (string) $title;
        $gag->image = $image;
        $gag->time = (string) $time;
        $gag->votes = (int) $votes;
        $gag->comments = (int) $comments;
        
        return $gag;
    }
}