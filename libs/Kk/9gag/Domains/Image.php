<?php

class Kk_9gag_Domains_Image
{
    public $thumb;
    public $small;
    public $big;
    
    public static function create($thumb, $small, $big)
    {
        $image = new Kk_9gag_Domains_Image();
        $image->thumb = (string) $thumb;
        $image->small = (string) $small;
        $image->big = (string) $big;
        
        return $image;
    }
}