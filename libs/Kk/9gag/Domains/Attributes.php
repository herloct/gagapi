<?php

class Kk_9gag_Domains_Attributes
{
    public $next;
    
    public static function create($next)
    {
        $attr = new Kk_9gag_Domains_Attributes();
        $attr->next = (float) $next;
        
        return $attr;
    }
}