<?php

class Kk_9gag_Domains_Gags implements Iterator, Countable
{
    public $attributes;
    public $images;
    
    public static function create(
        Kk_9gag_Domains_Attributes $attributes,
        array $images
    ) {
        $gags = new Kk_9gag_Domains_Gags;
        $gags->attributes = $attributes;
        $gags->images = $images;
        
        return $gags;
    }
    
    // Countable
    
    function count()
    {
        return count((array) $this->images);
    }
    
    // Iterator
    
    private $position = 0;
    
    function current()
    {
        return $this->images[$this->position];
    }
    
    function key()
    {
        return $this->position;
    }
    
    function next()
    {
        ++$this->position;
    }
    
    function rewind()
    {
        $this->position = 0;
    }
    
    function valid()
    {
        return isset($this->images[$this->position]);
    }
}