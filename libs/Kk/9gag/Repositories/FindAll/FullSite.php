<?php

class Kk_9gag_Repositories_FindAll_FullSite implements Kk_9gag_Repositories_FindAll_Interface
{
    public function findAll($section = Kk_9gag_Domains_Section::HOT, $page = 0)
    {
        $pagePath = '';
        if (0 != $page)
            $pagePath = "/id/{$page}";
        $url = "http://9gag.com/{$section}{$pagePath}";
        
        $client = new Zend_Http_Client($url);
        $response = $client->request();
        
        require 'phpQuery/phpQuery-onefile.php';
        phpQuery::newDocumentHTML($response->getBody());
        
        $images = array();
        foreach (pq("ul#entry-list-ul > li") as $li) {
            $li2 = pq($li);
            
            $id = (int) $li2->attr('gagid');
            
            $img = $li2->find('div.content img:first');
            $imgAlt = $img->attr('alt');
            if ('NSFW' == $imgAlt || ! $imgAlt)
                continue;
            
            $imgSmallUrl = $li2->find('div.content img:first')->attr('src');
            $imgUrl = dirname($imgSmallUrl);
            
            $images[] = Kk_9gag_Domains_Gag::create(
                $id,
                $li2->attr('data-url'),
                $li2->attr('data-text'),
                Kk_9gag_Domains_Image::create(
                    "{$imgUrl}/{$id}_220x145.jpg",
                    $imgSmallUrl,
                    "{$imgUrl}/{$id}_700b.jpg"
                ),
                '',
                $li2->find("span[votes]:first")->text(),
                trim($li2->find("span.comment:first")->text())
            );
        }
        $gags = Kk_9gag_Domains_Gags::create(
            Kk_9gag_Domains_Attributes::create(pq('div#pagination > a#next_button')->attr('data-more')),
            $images
        );
        
        return $gags;
    }
}