<?php

error_reporting(E_ALL);

// Define path to application directory
defined('APPLICATION_PATH')
 || define('APPLICATION_PATH', realpath(__DIR__));

// Define path to library directory
defined('APPLICATION_LIB')
 || define('APPLICATION_LIB', realpath(APPLICATION_PATH . '/../libs'));
 
defined('APPLICATION_ENV')
 || define('APPLICATION_ENV', 'testing');

// Include_path
set_include_path(implode(PATH_SEPARATOR, array(
  APPLICATION_LIB,
  get_include_path(),
)));

require_once('SplClassLoader.php');
$kkLoader = new SplClassLoader('Kk', APPLICATION_LIB);
$kkLoader->setNamespaceSeparator('_');
$kkLoader->register();

$zendLoader = new SplClassLoader('Zend', APPLICATION_LIB);
$zendLoader->setNamespaceSeparator('_');
$zendLoader->register();

$repository = new Kk_9gag_Repositories_FindAll_FullSite();
$action = new Kk_9gag_Actions_FindAll_Base($repository);

$useCache = isset($_GET['cache']) ? (bool) $_GET['cache'] : true;
if ($useCache) {
    $cache = Zend_Cache::factory(
        'Core',
        'Apc',
        array(
            'lifetime' => 60,
            'automatic_serialization' => true
        )
    );

    $action = new Kk_9gag_Actions_FindAll_ClientCached(new Kk_9gag_Actions_FindAll_Cached($action, $cache), 60);
}

$callback = isset($_GET['callback']) ? (string) $_GET['callback'] : null;
$page = isset($_GET['page']) ? (float) $_GET['page'] : 0;
$section = isset($_GET['section']) ? strtolower($_GET['section']) : Kk_9gag_Domains_Section::HOT;
if (! in_array($section, array(Kk_9gag_Domains_Section::HOT, Kk_9gag_Domains_Section::TRENDING)))
    $section = Kk_9gag_Domains_Section::HOT;
    
$gags = $action->findAll($section, $page);

if (is_null($callback)) {
    if (! headers_sent()) header('Content-Type: application/json');
    
    echo json_encode($gags);
} else {
    if (! headers_sent()) header('Content-Type: application/javascript');
    
    echo $callback, '(', json_encode($gags), ')';
}